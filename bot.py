#!/usr/bin/env python3.4
# pylint: disable=missing-docstring

from thetale import Game
from thetale.player import PlayerAbilities
from thetale.action import ActionTypes
from thetale.equipment import ArtifactEffects, ArtifactTypes

import logging
import sys

from time import monotonic, sleep
from datetime import datetime


formatter = logging.Formatter(
    fmt='%(asctime)s:%(process)d:%(name)s:%(levelname)s:%(message)s'
)

handler = logging.StreamHandler()
handler.setLevel(logging.DEBUG)
handler.setFormatter(formatter)

log_file = open('bot.log', 'w+')
handler2 = logging.StreamHandler(log_file)
handler2.setLevel(logging.DEBUG)
handler2.setFormatter(formatter)

logger = logging.getLogger('bot')
logger.setLevel(logging.DEBUG)
logger.addHandler(handler)
logger.addHandler(handler2)


sessionid = sys.argv[1] if len(sys.argv) > 1 else None
game = Game(sessionid=sessionid,
            client_id='bot.py-alpha')
(
    # pylint: disable=protected-access
    logger.info('Session ID: %s', game._sessionid)
)


basic_info_key = None
prev_time = None


reasons_to_help = (
    (
        'hero is being resurrected and is not too peaceful',
        lambda basic_info, info: (
            info.energy.regular > basic_info.abilities_cost[PlayerAbilities.help]
            and info.hero.traits.peacefulness <= -999
            and info.hero.action.type is ActionTypes.resurrection
        )
    ),
    (
        'hero is idle and not too peaceful',
        lambda basic_info, info: (
            info.energy.regular > basic_info.abilities_cost[PlayerAbilities.help]
            and info.hero.traits.peacefulness <= -999
            and info.hero.action.type is ActionTypes.inaction
        )
    ),
    (
        'hero has children present',
        lambda basic_info, info: (
            info.energy.regular > basic_info.abilities_cost[PlayerAbilities.help]
            and ArtifactEffects.children_present in info.bag
        )
    ),
    (
        'hero is too peaceful and battling',
        lambda basic_info, info: (
            info.energy.regular >= 8
            and info.hero.traits.peacefulness > -999
            and info.hero.action.type is ActionTypes.battle
        )
    ),
    (
        'hero is in city and is not too peaceful',
        lambda basic_info, info: (
            info.energy.regular > 12
            and info.hero.traits.peacefulness <= -999
            and info.hero.action.type in (
                ActionTypes.city,
                ActionTypes.trade,
            )
        ),
    ),
    (
        'hero has too much energy and is trying to get more',
        lambda basic_info, info: (
            info.energy.regular + 2 > info.energy.maximum
            and info.hero.action.type in (
                ActionTypes.energy_restoration,
            )
        )
    ),
    (
        'hero is in battle and has low health',
        lambda basic_info, info: (
            info.energy.regular >= 8
            and info.hero.action.type is ActionTypes.battle
            and info.hero.health / info.hero.max_health < 0.10
        )
    ),
    (
        'hero is taking care of his companion which has low health',
        lambda basic_info, info: (
            info.energy.regular >= 4
            and info.hero.action.type is ActionTypes.companion_care
            and info.companion.health / info.companion.max_health <= 0.5
        )
    ),
    (
        'hero is taking care of his companion which has too low health',
        lambda basic_info, info: (
            info.hero.action.type is ActionTypes.companion_care
            and info.companion.health / info.companion.max_health <= 0.2
        )
    ),
)


prev_turn = None
turn_length_adj = 0.0
max_adj = 10
min_adj = -max_adj
prev_infos = []
expiration_time = datetime.now()
while True:
    # pylint: disable=broad-except
    try:
        logger.debug('Obtaining game information')
        info = game.info(prev_infos=prev_infos)
    except Exception as e:
        logger.exception(
            'Unexpected exception while trying to get info: %s', str(e))
        continue
    if not info.account or datetime.now() >= expiration_time:
        logger.debug('Trying to authorise')
        astate = game.authorise(
            print,
            application_name='bot-0.0',
            application_info='the-tale.org bot',
            application_description='the-tale.org bot',
        ).result()
        expiration_time = astate.expiration_time
        continue
    try:
        patch_turn = info.response['data']['account']['hero']['patch_turn']
        if patch_turn:
            logger.debug('Patch turn: %i', patch_turn)
        if len(prev_infos) > 2:
            prev_infos.pop(0)
        prev_infos.append(info)
        logger.info('Current hero action: %s', info.hero.action.type.name)
        new_basic_info_key = info.equipment
        if basic_info_key != new_basic_info_key:
            logger.debug('Obtaining basic game information')
            basic_info = game.basic_info()
            max_adj = basic_info.turn_length / 3
            min_adj = -max_adj
            basic_info_key = new_basic_info_key
        cur_turn = info.game.turn.number
        if prev_turn is not None and cur_turn != prev_turn + 1:
            turn_length_adj += 0.1 * ((prev_turn + 1) - cur_turn)
            turn_length_adj = min(turn_length_adj, max_adj)
            turn_length_adj = max(turn_length_adj, min_adj)
            logger.debug('Turn length adjust: %f', turn_length_adj)
        prev_turn = cur_turn
    except Exception as e:
        logger.exception(
            'Unexpected exception while trying to process info: %s', str(e))
    else:
        if info.energy > basic_info.abilities_cost[PlayerAbilities.help]:
            for reason_desc, reason_func in reasons_to_help:
                if reason_func(basic_info, info):
                    # pylint: disable=broad-except
                    try:
                        logger.info('Helping the hero because %s', reason_desc)
                        game.use_ability(PlayerAbilities.help)
                    except Exception as e:
                        logger.exception(
                            'Unexpected exception while trying to help: %s',
                            str(e))
        if info.energy.regular > basic_info.abilities_cost[PlayerAbilities.drop_item]:
            # pylint: disable=bad-continuation
            if (
                len(info.bag) == info.bag.max_size
                and info.hero.action.type is ActionTypes.battle
                and ArtifactTypes.trash in info.bag
                and ArtifactEffects.children_present not in info.bag
            ):
                # pylint: disable=broad-except
                try:
                    logger.info('Dropping item')
                    game.use_ability(PlayerAbilities.drop_item)
                except Exception as e:
                    logger.exception(
                        'Unexpected exception while trying to drop item: %s',
                        str(e))
        if info.cards.cards_help_count >= info.cards.cards_help_barrier:
            try:
                logger.info('Taking card')
                game.take_card()
            except Exception as e:
                logger.exception(
                    'Unexpected exception while trying to take card: %s',
                    str(e))
        if prev_time is not None:
            next_time = monotonic()
            sleep(max(basic_info.turn_length
                      - (next_time - prev_time)
                      + turn_length_adj, 0.1))
        else:
            sleep(basic_info.turn_length)
        prev_time = monotonic()
